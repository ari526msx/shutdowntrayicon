unit srcMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ImgList, ComCtrls, Registry;

type
  TfrmOption = class(TForm)
    TrayIcon1: TTrayIcon;
    PopupMenu1: TPopupMenu;
    menuLogoff: TMenuItem;
    menuSleep: TMenuItem;
    menuRestart: TMenuItem;
    menuPowerOff: TMenuItem;
    menuShutdown: TMenuItem;
    menuOption: TMenuItem;
    menuExit: TMenuItem;
    ImageList1: TImageList;
    N1: TMenuItem;
    ListView1: TListView;
    PopupMenu2: TPopupMenu;
    menuOptionVisible: TMenuItem;
    menuOptionDefault: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure menuExitClick(Sender: TObject);
    procedure menuOptionClick(Sender: TObject);
    procedure menuLogoffClick(Sender: TObject);
    procedure menuSleepClick(Sender: TObject);
    procedure menuRestartClick(Sender: TObject);
    procedure menuPowerOffClick(Sender: TObject);
    procedure menuShutdownClick(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure menuOptionVisibleClick(Sender: TObject);
    procedure menuOptionDefaultClick(Sender: TObject);
    procedure ListView1Edited(Sender: TObject; Item: TListItem; var S: string);
  private
    { Private 宣言 }
    procedure PowerOptions(opt: Cardinal);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public 宣言 }
  end;

const
  SE_SHUTDOWN_NAME = 'SeShutdownPrivilege';
  AppKey = '\Software\ShutdownTrayIcon';

var
  frmOption: TfrmOption;

implementation

{$R *.dfm}

var
  sDefaultTitle: string;

procedure TfrmOption.PowerOptions(opt: Cardinal);
var
  hToken: THandle;
  tkp, origintkp: TTokenPrivileges;
  ReturnLen: DWORD;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    OpenProcessToken(GetCurrentProcess,
                     TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken);

    //特権名を示すローカルな一意識別子(LUID)を取得する
    LookupPrivilegeValue(nil, SE_SHUTDOWN_NAME, tkp.Privileges[0].Luid);

    //Shutdown特権を有効にする
    tkp.PrivilegeCount := 1;
    //Shutdown特権を有効にする
    tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;

    //トークンのShutdown権限を有効にする
    AdjustTokenPrivileges(hToken, False, tkp,
                          Sizeof(TTokenPrivileges), origintkp, ReturnLen);
  end;

  //シャットダウン
  ExitWindowsEx(opt,0);
end;

procedure TfrmOption.TrayIcon1DblClick(Sender: TObject);
var
  ii: Integer;
begin
  //PowerOptions($00400000);
  for ii := 0 to ListView1.Items.Count-1 do begin
    if ListView1.Items[ii].SubItems[1] = 'はい' then begin
      PopupMenu1.Items[ii].OnClick(Sender);
      Break;
    end;
  end;
end;

//=============================================================================
//  CreateParamsメソッド処理
//  このメインフォーム(Form1)は起動時に表示しない
//=============================================================================
procedure TfrmOption.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Application.ShowMainForm := False;
end;

//=============================================================================
//  フォーム生成時の処理
//  トレイアイコンを表示する
//  ポップアップメニューを割り当てる
//=============================================================================
procedure TfrmOption.FormClose(Sender: TObject; var Action: TCloseAction);
var
  RegIniFile: TRegIniFile;
  ii: Integer;
begin
  RegIniFile := TRegIniFile.Create(AppKey);
  for ii := 1 to ListView1.Items.Count do begin
    RegIniFile.WriteString('menu' + IntToStr(ii), 'Caption', ListView1.Items[ii-1].Caption);
    RegIniFile.WriteString('menu' + IntToStr(ii), 'Visible', ListView1.Items[ii-1].SubItems[0]);
    RegIniFile.WriteString('menu' + IntToStr(ii), 'Default', ListView1.Items[ii-1].SubItems[1]);
  end;
end;

procedure TfrmOption.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if frmOption.Visible then begin
    frmOption.Visible := False;
    CanClose := False;
  end;

end;

procedure TfrmOption.FormCreate(Sender: TObject);
var
  RegIniFile: TRegIniFile;
  ii: Integer;
begin
  ii := 0;
  ListView1.Items.Add;
  ListView1.Items[ii].Caption := 'ログオフ';
  ListView1.Items[ii].SubItems.Add('表示する');
  ListView1.Items[ii].SubItems.Add('いいえ');

  ii := 1;
  ListView1.Items.Add;
  ListView1.Items[ii].Caption := 'スリープ';
  ListView1.Items[ii].SubItems.Add('表示する');
  ListView1.Items[ii].SubItems.Add('いいえ');

  ii := 2;
  ListView1.Items.Add;
  ListView1.Items[ii].Caption := '再起動';
  ListView1.Items[ii].SubItems.Add('表示する');
  ListView1.Items[ii].SubItems.Add('いいえ');

  ii := 3;
  ListView1.Items.Add;
  ListView1.Items[ii].Caption := 'PCの電源を切る';
  ListView1.Items[ii].SubItems.Add('表示する');
  ListView1.Items[ii].SubItems.Add('いいえ');

  ii := 4;
  ListView1.Items.Add;
  ListView1.Items[ii].Caption := 'シャットダウン';
  ListView1.Items[ii].SubItems.Add('表示する');
  ListView1.Items[ii].SubItems.Add('はい');

  RegIniFile := TRegIniFile.Create(AppKey);
  for ii := 0 to ListView1.Items.Count-1 do begin
    ListView1.Items[ii].Caption := RegIniFile.ReadString('menu' + IntToStr(ii+1), 'Caption', ListView1.Items[ii].Caption);
    ListView1.Items[ii].SubItems[0] := RegIniFile.ReadString('menu' + IntToStr(ii+1), 'Visible', ListView1.Items[ii].SubItems[0]);
    ListView1.Items[ii].SubItems[1] := RegIniFile.ReadString('menu' + IntToStr(ii+1), 'Default', ListView1.Items[ii].SubItems[1]);

    PopupMenu1.Items[ii].Caption := ListView1.Items[ii].Caption;
    if ListView1.Items[ii].SubItems[0] = '表示しない' then begin
      PopupMenu1.Items[ii].Visible := False;
    end;
    if ListView1.Items[ii].SubItems[1] = 'はい' then begin
      PopupMenu1.Items[ii].Checked := True;
      sDefaultTitle := ListView1.Items[ii].Caption;
    end;
  end;
  Self.BorderStyle    := bsDialog;
  TrayIcon1.Visible   := True;
  TrayIcon1.Hint      := frmOption.Caption + #13#10 + 'ここから' + sDefaultTitle +'できます。' + #13#10 + 'ダブルクリックで即実行します。';
  TrayIcon1.PopupMenu := PopupMenu1;
end;

procedure TfrmOption.ListView1Edited(Sender: TObject; Item: TListItem;
  var S: string);
begin
  PopupMenu1.Items[Item.Index].Caption := S;
  if PopupMenu1.Items[Item.Index].Checked then begin
    sDefaultTitle := S;
    TrayIcon1.Hint      := frmOption.Caption + #13#10 + 'ここから' + sDefaultTitle +'できます。' + #13#10 + 'ダブルクリックで即実行します。';
  end;
end;

procedure TfrmOption.menuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmOption.menuOptionClick(Sender: TObject);
begin
  frmOption.Visible := True;
end;

//表示・非表示切り替え
procedure TfrmOption.menuOptionVisibleClick(Sender: TObject);
var
  bVisible: Boolean;
begin
  bVisible := True;
  if ListView1.Selected.SubItems[0] = '表示する' then begin
    if ListView1.Selected.SubItems[1] = 'はい' then begin
      ShowMessage('非表示は、デフォルト以外の項目に設定できます。');
      Exit;
    end;
    ListView1.Selected.SubItems[0] := '表示しない';
    bVisible := False;
  end
  else begin
    ListView1.Selected.SubItems[0] := '表示する';
  end;
  PopupMenu1.Items[Listview1.Selected.Index].Visible := bVisible;
end;

//デフォルトにする
procedure TfrmOption.menuOptionDefaultClick(Sender: TObject);
var
  ii: Integer;
begin
  if ListView1.Selected.SubItems[0] = '表示しない' then begin
    ShowMessage('デフォルトは、表示する項目に設定できます。');
    Exit;
  end;
  for ii := 0 to ListView1.Items.Count-1 do begin
    ListView1.Items[ii].SubItems[1] := 'いいえ';
    PopupMenu1.Items[ii].Checked := False;
  end;
  ListView1.Selected.SubItems[1] := 'はい';
  PopupMenu1.Items[Listview1.Selected.Index].Checked := True;
  sDefaultTitle := ListView1.Selected.Caption;
  TrayIcon1.Hint      := frmOption.Caption + #13#10 + 'ここから' + sDefaultTitle +'できます。' + #13#10 + 'ダブルクリックで即実行します。';
end;

procedure TfrmOption.menuLogoffClick(Sender: TObject);
begin
  PowerOptions(EWX_LOGOFF);
end;

procedure TfrmOption.menuPowerOffClick(Sender: TObject);
begin
  PowerOptions(EWX_POWEROFF);
end;

procedure TfrmOption.menuRestartClick(Sender: TObject);
begin
  PowerOptions(EWX_REBOOT);
end;

procedure TfrmOption.menuShutdownClick(Sender: TObject);
begin
  PowerOptions($00400000);
end;

procedure TfrmOption.menuSleepClick(Sender: TObject);
begin
  PowerOptions(EWX_SHUTDOWN);
end;

end.
