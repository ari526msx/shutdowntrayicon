program ShutdownTrayIcon;

uses
  Forms,
  srcMain in 'srcMain.pas' {frmOption};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '電源オプション';
  Application.CreateForm(TfrmOption, frmOption);
  Application.Run;
end.
